<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::get('users', 'UsersController@index');
Route::get('users/list', 'UsersController@fetchUsers');
Route::delete('users/{id}', 'UsersController@delete');
Route::post('messages', 'ChatsController@sendMessage');
